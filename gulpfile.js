const gulp = require('gulp');
const rollup = require('rollup-stream');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

var options = {
    basePath : './src/js',
    format: 'iife',
    distPath: './dist/js',
}

gulp.task('rollup', function() {
    return rollup({
        input: options.basePath + '/app.js',
        format: options.format,
    })
    // point to the entry file.
    .pipe(source('bundle.js', options.basePath))
    // we need to buffer the output, since many gulp plugins don't support streams.
    .pipe(buffer())
    // some transformations like uglify, rename, etc.
    .pipe(gulp.dest(options.distPath));
});

gulp.task('moveHtml', function() {
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['rollup','moveHtml']);